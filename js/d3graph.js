
/**
 * @file
 * @author metzlerd
 * Sample D3.js graph
 * 
 * Drupal behaviors are the standard way to deal with javascript.  The reason that you'd use this 
 * as opposed to adding ready document event handlers or overriding js, is that these will be called even when 
 * new content is loaded by drupal ajax calls such as form reloads or overlays.  JQuery document ready events are
 * still suitable, but don't try and override document.ready as it will break your drupal site ;). 
 * 
 * See https://www.drupal.org/node/756722 for additional information. 
 * 
 */
(function ($) {
  // Change the name of the following property to match your module or report.  It just must be unique
  // and not conflic with other module provided methods. 
  Drupal.behaviors.frx3dGraph = {
    attach: function (context, settings) {
      var data = Drupal.settings.forenaData.usersByState; 
      // convert to numbers; 
      data.forEach(function(d) {
        d.total = +d.total;
      });
      
      var height = 600; 
      var width = 800; 
      var svg = d3.select('#chart')
        .append('svg')
        .attr("width", "800px")
        .attr("height", "600px"); 
    
      // Set base scales
      var x = d3.scale.ordinal()
      .rangeRoundBands([0, width], .1);

      var y = d3.scale.linear()
      .range([height, 0]);
     
    
      x.domain(data.map(function(d) { return d.state; }));
      y.domain([0, d3.max(data, function(d) { return d.total; })]);

      // Draw the bars for the graph. 
      svg.selectAll(".bar")
         .data(data)
        .enter().append("rect")
          .attr("class", "bar")
          .attr("x", function(d) { return x(d.state); })
          .attr("width", x.rangeBand())
          .attr("y", function(d) { return y(d.total); })
          .attr("height", function(d) { return height - y(d.total);
          }); 

    }
  };

})(jQuery);